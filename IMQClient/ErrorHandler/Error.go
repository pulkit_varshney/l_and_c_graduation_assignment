package error

import (
	"fmt"
	"os"
)

// CheckError ...
func CheckError(err error, message string) {
	if err != nil {
		fmt.Println(message, err)
		os.Exit(1)
	}
}
