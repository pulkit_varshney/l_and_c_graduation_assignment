package imqmodel

// IMQModel ...
type IMQModel struct {
	SourceURI      string `json:"soureURI" bson:"sourceURI"`
	DestinationURI string `json:"destinationURI" bson:"destinationURI"`
	Version        int    `json:"version" bson:"version"`
	Data           string `json:"data" bson:"data"`
}

// Topic ...
type Topic struct {
	TopicName string `json:"topicname" bson:"topicname"`
}

// TopicQueueModel ...
type TopicQueueModel struct {
	TopicName string `json:"topicname" bson:"topicname"`
	TopicID   string `json:"topicid" bson:"topicid"`
	QueueName string `json:"queuename" bson:"queuename"`
	QueueID   string `json:"queueid" bson:"queueid"`
}

//MessageModel ...
type MessageModel struct {
	Data []string `json:"data" bson:"data"`
}

// UserTypeModel ...
type UserTypeModel struct {
	UserType string `json:"usertype" bson:"usertype"`
}
