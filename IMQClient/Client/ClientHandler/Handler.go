package imqclient

import (
	"encoding/gob"
	"net"
	"strings"

	Error "github.com/pulkitvarshney/l_and_c_graduation_assignment/IMQClient/ErrorHandler"
	IMQModel "github.com/pulkitvarshney/l_and_c_graduation_assignment/IMQClient/IMQModel"
)

// Dial ...
func Dial(address string) net.Conn {
	clientConn, err := net.Dial("tcp", address)
	Error.CheckError(err, "unable to Connect Client")
	return clientConn
}

// TopicReceivingFromServer ...
func TopicReceivingFromServer(clientConn net.Conn) *[]IMQModel.Topic {
	dec := gob.NewDecoder(clientConn)
	topic := &[]IMQModel.Topic{}
	dec.Decode(topic)
	return topic
}

// SendUserTypeToServer ...
func SendUserTypeToServer(clientConn net.Conn, userType string) {
	formattedUserType := strings.TrimSpace(string(userType))
	encoder := gob.NewEncoder(clientConn)
	IMQModel := IMQModel.UserTypeModel{
		UserType: formattedUserType,
	}
	encoder.Encode(IMQModel)
}
