package imqclient_test

import (
	"fmt"
	"net"
	"testing"

	IMQClient "github.com/pulkitvarshney/l_and_c_graduation_assignment/IMQClient/Client/ClientHandler"
)

var address = "172.18.74.193:8080"

func mockServer(address string) error {
	ln, err := net.Listen("tcp", address)
	go func() {
		defer ln.Close()
		_, err = ln.Accept()
	}()
	return err
}

func mockConnection(address string) (client, server net.Conn, ln net.Listener) {
	//message := []byte("testing Message")
	ln, err := net.Listen("tcp", address)
	go func() {
		defer ln.Close()
		server, err = ln.Accept()
	}()
	client, err = net.Dial("tcp", ln.Addr().String())
	if err != nil {
		fmt.Print(err)
	}
	//client.Write(message)
	return client, server, ln
}
func TestDial_Success(t *testing.T) {
	mockServer(address)
	conn := IMQClient.Dial(address)
	if conn == nil {
		t.Fatalf("connection should not be nil. got=%v", conn)
	}
}
