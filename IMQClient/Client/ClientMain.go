package main

import (
	"bufio"
	"fmt"
	"os"
	"strings"

	Publisher "github.com/pulkitvarshney/l_and_c_graduation_assignment/IMQClient/Services/Publisher"
	Subscriber "github.com/pulkitvarshney/l_and_c_graduation_assignment/IMQClient/Services/Subscriber"
)

func main() {
	fmt.Println("1. Publisher\n2. Subscriber")
	inputReader := bufio.NewReader(os.Stdin)
	clientChoice, _ := inputReader.ReadString('\n')
	if clientChoice != "Publisher" && clientChoice != "Subscriber" {
		fmt.Println("Wrong Choice")
	}
	switch strings.TrimSpace(string(clientChoice)) {
	case "Publisher":
		{
			Publisher.PublisherService()
		}
	case "Subscriber":
		{
			Subscriber.SubscriberService()
		}

	}
}
