package subscriber

import (
	"encoding/gob"
	"net"

	IMQModel "github.com/pulkitvarshney/l_and_c_graduation_assignment/IMQClient/IMQModel"
)

// SendTopicNameToServer ...
func sendUserInfoAndTopicNameToServer(clientConn net.Conn, usernameWithSelectedTopiName string) {
	//	formattedTopicName := strings.TrimSpace(string(usernameWithSelectedTopiName))
	encoder := gob.NewEncoder(clientConn)
	IMQModel := IMQModel.Topic{
		TopicName: usernameWithSelectedTopiName,
	}
	encoder.Encode(IMQModel)
}

// DataCommingFromTopic ...
func dataCommingFromTopic(clientConn net.Conn) []string {
	dec := gob.NewDecoder(clientConn)
	var mess []string
	dec.Decode(&mess)
	return mess
}
