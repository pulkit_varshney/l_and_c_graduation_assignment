package subscriber

import (
	"bufio"
	"fmt"
	"os"

	IMQClient "github.com/pulkitvarshney/l_and_c_graduation_assignment/IMQClient/Client/ClientHandler"
	Configs "github.com/pulkitvarshney/l_and_c_graduation_assignment/IMQClient/Configs"
	Error "github.com/pulkitvarshney/l_and_c_graduation_assignment/IMQClient/ErrorHandler"
)

// SubscriberService ...
func SubscriberService() {
	clientNameMessage := "Please enter your name"
	address := Configs.IP + ":" + Configs.Port
	clientConn := IMQClient.Dial(address)
	IMQClient.SendUserTypeToServer(clientConn, "Subscriber")
	fmt.Println(clientNameMessage)
	inputReader := bufio.NewReader(os.Stdin)
	subscriberName, err := inputReader.ReadString('\n')
	if err != nil {
		Error.CheckError(err, "unable to read message")
	}
	fetchTopic := IMQClient.TopicReceivingFromServer(clientConn)
	fmt.Println("Select Topic From the List")
	fmt.Println(*fetchTopic)
	topicName, err := inputReader.ReadString('\n')
	if err != nil {
		Error.CheckError(err, "unable to read message")
	}
	usernameWithSelectedTopiName := subscriberName + "," + topicName
	sendUserInfoAndTopicNameToServer(clientConn, usernameWithSelectedTopiName)
	messageModel := dataCommingFromTopic(clientConn)
	for index := 0; index < len(messageModel); index++ {
		fmt.Println(messageModel[index])
	}
	if messageModel == nil {
		fmt.Println("No new message is available")
	}
}
