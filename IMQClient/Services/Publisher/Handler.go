package publisher

import (
	"bufio"
	"encoding/gob"
	"net"
	"os"

	configs "github.com/pulkitvarshney/l_and_c_graduation_assignment/IMQClient/Configs"
	Error "github.com/pulkitvarshney/l_and_c_graduation_assignment/IMQClient/ErrorHandler"
	IMQModel "github.com/pulkitvarshney/l_and_c_graduation_assignment/IMQClient/IMQModel"
)

// WriteMessage ...
func writeMessage(clientConn net.Conn, topicName string) string {
	inputReader := bufio.NewReader(os.Stdin)
	inputMessage, err := inputReader.ReadString('\n')
	if err != nil {
		Error.CheckError(err, "unable to read message")
	}
	messagewithTopicName := inputMessage + "," + topicName
	encoder := gob.NewEncoder(clientConn)
	IMQModel := IMQModel.IMQModel{
		SourceURI:      "",
		DestinationURI: getServerIP(clientConn),
		Version:        int(configs.Version),
		Data:           messagewithTopicName,
	}
	encoder.Encode(IMQModel)
	return inputMessage
}

// GetServerIP ...
func getServerIP(clientConn net.Conn) string {
	serverIP := clientConn.RemoteAddr().String()
	return serverIP
}

// ClientName ...
func clientName(clientConn net.Conn) {
	inputReader := bufio.NewReader(os.Stdin)
	clientName, err := inputReader.ReadString('\n')
	if err != nil {
		Error.CheckError(err, "unable to read message")
	}
	encoder := gob.NewEncoder(clientConn)
	IMQModel := IMQModel.IMQModel{
		SourceURI:      "",
		DestinationURI: getServerIP(clientConn),
		Version:        int(configs.Version),
		Data:           clientName,
	}
	encoder.Encode(IMQModel)
}

// MessageReceivingFromServer ...
func messageReceivingFromServer(clientConn net.Conn) *IMQModel.IMQModel {
	dec := gob.NewDecoder(clientConn)
	IMQModel := &IMQModel.IMQModel{}
	dec.Decode(IMQModel)
	return IMQModel
}
