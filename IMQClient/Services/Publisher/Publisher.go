package publisher

import (
	"bufio"
	"fmt"
	"os"
	"strings"

	IMQClient "github.com/pulkitvarshney/l_and_c_graduation_assignment/IMQClient/Client/ClientHandler"
	Configs "github.com/pulkitvarshney/l_and_c_graduation_assignment/IMQClient/Configs"
	Error "github.com/pulkitvarshney/l_and_c_graduation_assignment/IMQClient/ErrorHandler"
)

// PublisherService ...
func PublisherService() {
	clientMessageToExit := "STOP"
	clientNameMessage := "Please enter your name"
	address := Configs.IP + ":" + Configs.Port
	clientConn := IMQClient.Dial(address)
	IMQClient.SendUserTypeToServer(clientConn, "Publisher")
	serverIP := getServerIP(clientConn)
	fmt.Println("Server IP is ", serverIP)
	fmt.Println(clientNameMessage)
	clientName(clientConn)
	fetchTopic := IMQClient.TopicReceivingFromServer(clientConn)
	fmt.Println("Select Topic From the List")
	fmt.Println(*fetchTopic)
	inputReader := bufio.NewReader(os.Stdin)
	TopicName, err := inputReader.ReadString('\n')
	if err != nil {
		Error.CheckError(err, "unable to read Topic Name")
	}
	for {
		fmt.Println("Enter message to send or Enter STOP to exit")
		clientMessage := writeMessage(clientConn, TopicName)
		IMQModel := messageReceivingFromServer(clientConn)
		fmt.Println("Message from Server", IMQModel.Data)
		if strings.TrimSpace(string(clientMessage)) == clientMessageToExit {
			fmt.Println("TCP client exiting...")
			return
		}
	}
}
