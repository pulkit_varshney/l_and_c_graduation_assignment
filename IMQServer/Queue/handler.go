package queue

import (
	"fmt"
	"time"

	"github.com/google/uuid"
	configs "github.com/pulkitvarshney/l_and_c_graduation_assignment/IMQServer/Configs"
	IMQModel "github.com/pulkitvarshney/l_and_c_graduation_assignment/IMQServer/IMQModel"
	Storage "github.com/pulkitvarshney/l_and_c_graduation_assignment/IMQServer/Storage"
	"gopkg.in/mgo.v2"
)

func getSession() *mgo.Session {
	session, err := mgo.Dial(configs.DatabaseConnector)

	if err != nil {
		panic(err)
	}
	return session
}

// TopicMappingWithQueue ...
func TopicMappingWithQueue() {
	uc := Storage.DatabaseController(getSession())
	topics := uc.FetchAllTopics()
	queueID := uuid.New().String()
	for index := 0; index < len(topics); index++ {
		queueTopicModel := IMQModel.TopicQueueModel{
			TopicName: topics[index].TopicName,
			TopicID:   uuid.New().String(),
			QueueName: "IMQQueue",
			QueueID:   queueID,
		}
		uc.QueueTopicMapping(queueTopicModel)
	}
}

// StoreMessageInQueue ...
func StoreMessageInQueue(topicName, clientMessage, userName string) error {
	uc := Storage.DatabaseController(getSession())
	queueData, err := uc.GetQueueData(topicName)
	if queueData == nil {
		return err
	}
	// In 30 seconds, the message will expired
	location, _ := time.LoadLocation("UTC")
	createdTime := time.Now().In(location)
	expireTime := time.Now().In(location).Add(time.Second * time.Duration(30))
	messageModel := []IMQModel.MessageInfoModel{{
		MessageID:   uuid.New().String(),
		MessageData: clientMessage,
		CreatedTime: createdTime,
		ExpireTime:  expireTime,
	}}
	MessageDataModel := IMQModel.QueueMessageModel{
		TopicID:  queueData[0].TopicID,
		QueueID:  queueData[0].QueueID,
		UserName: userName,
		Messages: messageModel,
	}

	err = uc.InsertDataInQueue(MessageDataModel)
	if err != nil {
		fmt.Println("unable to store message in database", err)
	}
	return err
}
