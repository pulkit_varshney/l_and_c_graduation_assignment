package imqserver_test

import (
	"fmt"
	"net"
	"testing"

	Configs "github.com/pulkitvarshney/l_and_c_graduation_assignment/IMQServer/Configs"
	IMQServer "github.com/pulkitvarshney/l_and_c_graduation_assignment/IMQServer/Server/ServerHandler"
)

func mockConnection() (client, server net.Conn, ln net.Listener) {
	//message := []byte("testing Message")
	ln, err := net.Listen("tcp", "127.0.0.1:0")
	go func() {
		defer ln.Close()
		server, err = ln.Accept()
	}()
	client, err = net.Dial("tcp", ln.Addr().String())
	if err != nil {
		fmt.Print(err)
	}
	//client.Write(message)
	return client, server, ln
}

func TestListener_Success(t *testing.T) {
	serverAddress := Configs.ServerIP + ":" + Configs.ServerPort
	listener := IMQServer.Listener(serverAddress)
	if listener == nil {
		t.Fatalf("listener should not be nil. got=%v", listener)
	}
}

func TestGetClientIP_Success(t *testing.T) {
	client, _, _ := mockConnection()
	clientIP := IMQServer.GetClientIP(client)
	if clientIP == "" {
		t.Fatalf("clientIP should not be nil. got=%v", clientIP)
	}
}

// func TestEstablishConnectionWithClient_Success(t *testing.T) {
// 	_, _, listener := mockConnection()
// 	_, err := IMQServer.EstablishConnectionWithClient(listener)
// 	if err != nil {
// 		t.Fatalf("err should be nil. got=%v", err)
// 	}
// }

func TestGetUniqueNumberFromClientIP_Success(t *testing.T) {
	_, _, ln := mockConnection()
	uniqueNumber := IMQServer.GetUniqueNumberFromClientIP(ln.Addr().String())
	if uniqueNumber == "" {
		t.Fatalf("uniqueNumber should not be nil. got=%v", uniqueNumber)
	}
}

func TestSendingMessageBackToClient_Success(t *testing.T) {
	conn, _, _ := mockConnection()
	IMQServer.SendingMessageBackToClient(conn, "testing message")
}

func TestGetSession_Success(t *testing.T) {
	session := IMQServer.GetSession()
	if session == nil {
		t.Fatalf("session should not be nil. got=%v", session)
	}
}
func TestHandleConnection_Success(t *testing.T) {
	conn, _, _ := mockConnection()
	err := IMQServer.PublisherHandler(conn, "testing")
	if err == nil {
		t.Fatalf("err should be nil. got=%v", err)
	}
}
