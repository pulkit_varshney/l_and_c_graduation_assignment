package imqserver

import (
	"encoding/gob"
	"fmt"
	"net"
	"strings"

	configs "github.com/pulkitvarshney/l_and_c_graduation_assignment/IMQServer/Configs"
	Error "github.com/pulkitvarshney/l_and_c_graduation_assignment/IMQServer/ErrorHandler"
	IMQModel "github.com/pulkitvarshney/l_and_c_graduation_assignment/IMQServer/IMQModel"
	Queue "github.com/pulkitvarshney/l_and_c_graduation_assignment/IMQServer/Queue"
	Storage "github.com/pulkitvarshney/l_and_c_graduation_assignment/IMQServer/Storage"
	"gopkg.in/mgo.v2"
)

var (
	flag = false
)

func databaseSession() *Storage.Controller {
	uc := Storage.DatabaseController(GetSession())
	return uc
}

// Listener ...
func Listener(address string) net.Listener {
	listener, err := net.Listen("tcp", address)
	Error.CheckError(err, "Unable to cconnect")
	return listener
}

// EstablishConnectionWithClient ...
func EstablishConnectionWithClient(listener net.Listener) (c net.Conn, err error) {
	serverConnection, err := listener.Accept()
	Error.CheckError(err, "Failed to establish Conection")

	return serverConnection, err
}

// GetClientIP ...
func GetClientIP(serverConn net.Conn) string {
	clientIP := serverConn.RemoteAddr().String()
	return clientIP
}

// PublisherHandler ...
func PublisherHandler(c net.Conn, clientName string) error {
	messageToCloseConnection := "STOP"
	fmt.Println("New Client Joins")
	SendingTopicsToClient(c)
	var err error
	for {
		// uc := Storage.DatabaseController(GetSession())
		unformattedMessage := ReadingMessage(c)
		message := strings.Split(unformattedMessage, ",")
		clientMessage := message[0]
		topicName := message[1]
		SendingMessageBackToClient(c, clientMessage)
		formattedClientMessage := strings.TrimSpace(string(clientMessage))
		formattedTopicName := strings.TrimSpace(string(topicName))
		formattedClientName := strings.TrimSpace(string(clientName))
		if formattedClientMessage == messageToCloseConnection {
			fmt.Println("Client stops of IP ", GetClientIP(c))
			break
		}
		err = Queue.StoreMessageInQueue(formattedTopicName, formattedClientMessage, formattedClientName)
		if err != nil {
			fmt.Println("unable to proceed with this topic")
		}
		clientIdentificationNumber := GetUniqueNumberFromClientIP(GetClientIP(c))
		fmt.Println("Client:", clientIdentificationNumber, formattedClientMessage)
	}
	c.Close()
	return err
}

// GetUniqueNumberFromClientIP ...
func GetUniqueNumberFromClientIP(ip string) string {
	clientUnquieNumber := strings.Split(ip, ":")
	return clientUnquieNumber[1]
}

// ReadingMessage ...
func ReadingMessage(c net.Conn) string {
	// clientMessage, err := bufio.NewReader(c).ReadString('\n')
	dec := gob.NewDecoder(c)
	IMQModel := &IMQModel.IMQModel{}
	dec.Decode(IMQModel)
	return IMQModel.Data
}

// GetSession ...
func GetSession() *mgo.Session {
	session, err := mgo.Dial(configs.DatabaseConnector)

	if err != nil {
		panic(err)
	}
	return session
}

// SendingMessageBackToClient ...
func SendingMessageBackToClient(c net.Conn, clientMessage string) {
	encoder := gob.NewEncoder(c)
	IMQModel := IMQModel.IMQModel{
		SourceURI:      "",
		DestinationURI: GetClientIP(c),
		Version:        int(configs.Version),
		Data:           clientMessage,
	}
	encoder.Encode(IMQModel)
}

// SendingTopicsToClient ...
func SendingTopicsToClient(c net.Conn) {
	encoder := gob.NewEncoder(c)
	topic := FetchAllTopics()
	encoder.Encode(topic)
}

// FetchAllTopics ...
func FetchAllTopics() []Storage.Topic {
	uc := Storage.DatabaseController(GetSession())
	// uc.CreateAndStoreTopics()
	topic := uc.FetchAllTopics()
	return topic
}

// SubscriberHandler ...
func SubscriberHandler(c net.Conn) {
	output := []string{"No new message is available"}
	var messageOutput []string
	var messageIDArray []string
	var messageIDToData = make(map[string]string)
	var flag int = 0
	var value string
	uc := databaseSession()
	SendingTopicsToClient(c)
	unformattedInfo := readingUserInfoAndFromSubscriber(c)
	formattedInfo := strings.Split(unformattedInfo, ",")
	clientName := formattedInfo[0]
	topicName := formattedInfo[1]
	formattedTopicName := strings.TrimSpace(string(topicName))
	formattedClientName := strings.TrimSpace(string(clientName))
	fmt.Println(formattedClientName)
	topicData := uc.GetTopicIDUsingTopicName(formattedTopicName)
	data := uc.GetDataFromTopic(topicData[0].TopicID)
	if data != nil {
		for index := 0; index < len(data); index++ {
			for jIndex := 0; jIndex < len(data[index].Messages); jIndex++ {
				messageIDArray = append(messageIDArray, data[index].Messages[jIndex].MessageID)
				messageIDToData[data[index].Messages[jIndex].MessageID] = data[index].Messages[jIndex].MessageData
			}
		}
		flag++
	}
	undeliveredMessageID := uc.CheckMessageIsAlreadyDeleivered(formattedClientName, messageIDArray)
	for index := 0; index < len(undeliveredMessageID); index++ {
		value = messageIDToData[undeliveredMessageID[index]]
		messageOutput = append(messageOutput, value)
	}

	encoder := gob.NewEncoder(c)
	if flag > 0 {
		encoder.Encode(messageOutput)

	} else {
		encoder.Encode(output)
	}
}

func readingUserInfoAndFromSubscriber(c net.Conn) string {
	dec := gob.NewDecoder(c)
	topicModel := &IMQModel.TopicModel{}
	dec.Decode(topicModel)
	return topicModel.TopicName
}

// ReadingUserType ...
func ReadingUserType(c net.Conn) string {
	dec := gob.NewDecoder(c)
	userTpeModel := &IMQModel.UserTypeModel{}
	dec.Decode(userTpeModel)
	return userTpeModel.UserType
}
