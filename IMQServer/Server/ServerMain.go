package main

import (
	"fmt"

	Configs "github.com/pulkitvarshney/l_and_c_graduation_assignment/IMQServer/Configs"
	DLQ "github.com/pulkitvarshney/l_and_c_graduation_assignment/IMQServer/DeadLetterQueue"
	Queue "github.com/pulkitvarshney/l_and_c_graduation_assignment/IMQServer/Queue"
	IMQServer "github.com/pulkitvarshney/l_and_c_graduation_assignment/IMQServer/Server/ServerHandler"
)

func main() {
	serverAddress := Configs.ServerIP + ":" + Configs.ServerPort
	fmt.Println("Server is Starting....")

	listener := IMQServer.Listener(serverAddress)
	defer listener.Close()
	Queue.TopicMappingWithQueue()

	for {
		serverConnection, err := IMQServer.EstablishConnectionWithClient(listener)
		if err == nil {
			fmt.Println("Connecting To Client....")
		}
		userType := IMQServer.ReadingUserType(serverConnection)
		switch userType {
		case "Publisher":
			{
				clientIP := IMQServer.GetClientIP(serverConnection)
				fmt.Println("Client IP is ", clientIP)
				clientName := IMQServer.ReadingMessage(serverConnection)
				fmt.Print(clientName)
				go IMQServer.PublisherHandler(serverConnection, clientName)
			}
		case "Subscriber":
			{
				DLQ.SendMessageToDLQ()
				clientIP := IMQServer.GetClientIP(serverConnection)
				fmt.Println("Client IP is ", clientIP)
				go IMQServer.SubscriberHandler(serverConnection)
			}
		}

	}
}
