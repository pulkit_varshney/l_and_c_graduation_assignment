package imqmodel

import "time"

// IMQModel ...
type IMQModel struct {
	SourceURI      string `json:"soureURI" bson:"sourceURI"`
	DestinationURI string `json:"destinationURI" bson:"destinationURI"`
	Version        int    `json:"version" bson:"version"`
	Data           string `json:"data" bson:"data"`
}

// TopicQueueModel ...
type TopicQueueModel struct {
	TopicName string `json:"topicname" bson:"topicname"`
	TopicID   string `json:"topicid" bson:"topicid"`
	QueueName string `json:"queuename" bson:"queuename"`
	QueueID   string `json:"queueid" bson:"queueid"`
}

// QueueMessageModel ...
type QueueMessageModel struct {
	TopicID  string             `json:"topicid" bson:"topicid"`
	QueueID  string             `json:"queueid" bson:"queueid"`
	UserName string             `json:"username" bson:"username"`
	Messages []MessageInfoModel `json:"messages" bson:"messages"`
}

// MessageInfoModel ...
type MessageInfoModel struct {
	MessageID   string    `json:"messageid" bson:"messageid"`
	MessageData string    `json:"messagedata" bson:"messagedata"`
	CreatedTime time.Time `json:"createdtime" bson:"createdtime"`
	ExpireTime  time.Time `json:"expiretime" bson:"expiretime"`
}

// TopicModel ...
type TopicModel struct {
	TopicName string `json:"topicname" bson:"topicname"`
}

// UserTypeModel ...
type UserTypeModel struct {
	UserType string `json:"usertype" bson:"usertype"`
}
