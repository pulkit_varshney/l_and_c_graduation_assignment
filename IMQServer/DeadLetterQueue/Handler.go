package deadletterqueue

import (
	"time"

	configs "github.com/pulkitvarshney/l_and_c_graduation_assignment/IMQServer/Configs"
	Storage "github.com/pulkitvarshney/l_and_c_graduation_assignment/IMQServer/Storage"
	"gopkg.in/mgo.v2"
)

// SendMessageToDLQ ...
func SendMessageToDLQ() {
	uc := databaseSession()
	messageComingFromDatabase := uc.GetAllTopicDataFromDataBase()
	for index := 0; index < len(messageComingFromDatabase); index++ {
		for jIndex := 0; jIndex < len(messageComingFromDatabase[index].Messages); jIndex++ {
			messageID := messageComingFromDatabase[index].Messages[jIndex].MessageID
			Expiredtime := messageComingFromDatabase[index].Messages[jIndex].ExpireTime
			isMessageExpired := toCheckMessageIsExpired(Expiredtime)
			if isMessageExpired == true {
				uc.AddMessageInDLQ(messageComingFromDatabase[index].Messages[jIndex])
				uc.RemoveMessageFromQueue(messageComingFromDatabase[index].TopicID, messageComingFromDatabase[index].UserName, messageID)
			}
		}
	}
}

// GetSession ...
func getSession() *mgo.Session {
	session, err := mgo.Dial(configs.DatabaseConnector)
	if err != nil {
		panic(err)
	}
	return session
}

func databaseSession() *Storage.Controller {
	uc := Storage.DatabaseController(getSession())
	return uc
}

func toCheckMessageIsExpired(expiredTime time.Time) bool {
	loc, _ := time.LoadLocation("Asia/Kolkata")
	now := time.Now().In(loc)
	diff := expiredTime.Sub(now)
	// hrs := int(diff.Hours())
	// mins := int(diff.Minutes())
	seconds := int(diff.Seconds())
	// if we are adding expired time is less than 60 seconds then only check second should less than zero,
	// if we are adding expired time is less than 60 minutes then check second and minutes should less than zero,
	// if we are adding expired time is less than 24 hours then check second, minutes and hours should less than zero,
	if seconds < 0 {
		return true
	}
	return false
}
