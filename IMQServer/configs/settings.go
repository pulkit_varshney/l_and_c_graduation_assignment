package configs

// Always have to check the ip of the local maching, If ip is differnet then change the ip here.
// Use command ipconfig to get the ip

var (
	// DatabaseConnector ...
	DatabaseConnector = "mongodb://localhost:27017/"
	// ServerPort ...
	ServerPort = "8080"
	//ServerIP ...
	ServerIP = ""
	// Version
	Version = 1.0
)
