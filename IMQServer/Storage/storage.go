package storage

import (
	"log"

	Error "github.com/pulkitvarshney/l_and_c_graduation_assignment/IMQServer/ErrorHandler"
	IMQModel "github.com/pulkitvarshney/l_and_c_graduation_assignment/IMQServer/IMQModel"
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

// Controller ...
type Controller struct {
	session *mgo.Session
}

// DatabaseController ...
func DatabaseController(s *mgo.Session) *Controller {
	return &Controller{s}
}

// StorageHandler ...
// func (uc Controller) StorageHandler(clientMessage, clientName string) error {
// 	var collectionName []string
// 	var clientFiltered []bson.M

// 	clientData := ClientData{
// 		Data:      clientMessage,
// 		SerialNo:  bson.NewObjectId(),
// 		Timestamp: time.Now(),
// 	}

// 	collectionName, err := uc.session.DB("ClientData").CollectionNames()
// 	if err != nil {
// 		Error.CheckError(err, "unable to get collections name")
// 	}
// 	clientNameData := Client{
// 		ClientName: clientName,
// 	}

// 	// Adding client data in the client table
// 	for index := 0; index < len(collectionName); index++ {
// 		if collectionName[index] != clientName {
// 			uc.session.DB("ClientData").C(clientName).Insert(clientData)
// 			break
// 		} else {
// 			uc.session.DB("ClientData").C(clientName).Insert(clientData)
// 			break
// 		}
// 	}

// 	rootTablesession := uc.session.DB("ClientData").C("ClientsInfo")
// 	findClient := rootTablesession.Find(bson.M{"clientName": clientName})
// 	if err = findClient.All(&clientFiltered); err != nil {
// 		log.Fatal(err)
// 	}

// 	// Adding client name in the root table
// 	if len(clientFiltered) == 0 {
// 		uc.session.DB("ClientData").C("ClientsInfo").Insert(clientNameData)
// 	}
// 	return err
// }

// FetchAllTopics ...
func (uc Controller) FetchAllTopics() []Topic {
	result := []Topic{}
	err := uc.session.DB("ClientData").C("Topics").Find(nil).All(&result)
	if err != nil {
		Error.CheckError(err, "unable to get topics name from database")
	}
	return result
}

// CreateAndStoreTopics - Function of Admin...
func (uc Controller) CreateAndStoreTopics() error {
	var err error
	topic := []string{"Sports", "Entertainment", "News", "History", "SciFi"}
	for index := 0; index < len(topic); index++ {
		topicName := Topic{
			TopicName: topic[index],
		}
		err = uc.session.DB("ClientData").C("Topics").Insert(topicName)
	}
	if err != nil {
		Error.CheckError(err, "unable to store topic in database")
	}
	return err
}

// QueueTopicMapping - Function of Admin...
func (uc Controller) QueueTopicMapping(topicQueueModel IMQModel.TopicQueueModel) {
	var topicFiltered []bson.M
	tablesession := uc.session.DB("ClientData").C("QueueTopicMapping")
	findTopic := tablesession.Find(bson.M{"topicname": topicQueueModel.TopicName})
	if err := findTopic.All(&topicFiltered); err != nil {
		log.Fatal(err)
	}
	if len(topicFiltered) == 0 {
		uc.session.DB("ClientData").C("QueueTopicMapping").Insert(topicQueueModel)
	}
}

// GetQueueData ...
func (uc Controller) GetQueueData(topicName string) ([]IMQModel.TopicQueueModel, error) {
	var result []IMQModel.TopicQueueModel
	queueColection := uc.session.DB("ClientData").C("QueueTopicMapping")
	data := queueColection.Find(bson.M{"topicname": topicName})
	err := data.All(&result)
	if result == nil {
		return nil, err
	}
	if err != nil {
		Error.CheckError(err, "unable to get topics name from database")
	}
	return result, nil
}

// InsertDataInQueue ...
func (uc Controller) InsertDataInQueue(messageDataModel IMQModel.QueueMessageModel) error {
	var result []IMQModel.TopicQueueModel
	var err error
	flag := 0
	userColection := uc.session.DB("ClientData").C("IMQ_Queue")
	data := userColection.Find(bson.M{"username": messageDataModel.UserName})
	err = data.All(&result)
	if err != nil {
		Error.CheckError(err, "unable to get topics name from database")
	}
	if len(result) > 0 {
		for index := 0; index < len(result); index++ {
			if result[index].TopicID == messageDataModel.TopicID {
				flag++
			}
		}
		if flag > 0 {
			query := bson.M{"topicid": messageDataModel.TopicID, "username": messageDataModel.UserName}
			update := bson.M{"$push": bson.M{"messages": messageDataModel.Messages[0]}}

			err = uc.session.DB("ClientData").C("IMQ_Queue").Update(query, update)
		} else {
			err = uc.session.DB("ClientData").C("IMQ_Queue").Insert(messageDataModel)
		}
	} else {
		err = uc.session.DB("ClientData").C("IMQ_Queue").Insert(messageDataModel)
	}
	return err
}

// GetTopicIDUsingTopicName ...
func (uc Controller) GetTopicIDUsingTopicName(topicName string) []IMQModel.TopicQueueModel {
	var result []IMQModel.TopicQueueModel
	queueColection := uc.session.DB("ClientData").C("QueueTopicMapping")
	data := queueColection.Find(bson.M{"topicname": topicName})
	err := data.All(&result)
	if err != nil {
		Error.CheckError(err, "unable to get topics data from database")
	}
	return result
}

// GetDataFromTopic ...
func (uc Controller) GetDataFromTopic(topicID string) []IMQModel.QueueMessageModel {
	var result []IMQModel.QueueMessageModel
	queueColection := uc.session.DB("ClientData").C("IMQ_Queue")
	data := queueColection.Find(bson.M{"topicid": topicID})
	err := data.All(&result)
	if err != nil {
		Error.CheckError(err, "unable to get topics data from database")
	}
	if result == nil {
		return nil
	}
	return result
}

//CheckMessageIsAlreadyDeleivered ...
func (uc Controller) CheckMessageIsAlreadyDeleivered(clientName string, messageIDArray []string) []string {
	var undeliveredMessageID []string
	var result []MessageModel
	var deliveredMessage []string
	messageIDComingFromTopic := MessageModel{
		UserName:       clientName,
		MessageIDArray: messageIDArray,
	}
	subscriberColection := uc.session.DB("ClientData").C("Subscribers")
	data := subscriberColection.Find(bson.M{"username": clientName})
	err := data.All(&result)
	if err != nil {
		Error.CheckError(err, "unable to get subscriber name from database")
	}
	if len(result) > 0 {
		for index := 0; index < len(result); index++ {
			for jIndex := 0; jIndex < len(result[index].MessageIDArray); jIndex++ {
				deliveredMessage = append(deliveredMessage, result[index].MessageIDArray[jIndex])
			}
		}
		query := bson.M{"username": clientName}
		undeliveredMessageID = undelivered(deliveredMessage, messageIDArray)
		update := bson.M{"$push": bson.M{"messageid": bson.M{"$each": undeliveredMessageID}}}
		uc.session.DB("ClientData").C("Subscribers").Update(query, update)
	} else {
		err = uc.session.DB("ClientData").C("Subscribers").Insert(messageIDComingFromTopic)
		return messageIDComingFromTopic.MessageIDArray
	}
	return undeliveredMessageID
}

// GetAllTopicDataFromDataBase ...
func (uc Controller) GetAllTopicDataFromDataBase() []IMQModel.QueueMessageModel {
	var result []IMQModel.QueueMessageModel
	err := uc.session.DB("ClientData").C("IMQ_Queue").Find(nil).All(&result)
	if err != nil {
		Error.CheckError(err, "unable to get all data from database")
	}
	return result
}

// RemoveMessageFromQueue ...
func (uc Controller) RemoveMessageFromQueue(topicID, username, messageID string) error {
	//	messageArray := []string{messageID}
	queueColection := uc.session.DB("ClientData").C("IMQ_Queue")
	query := bson.M{"topicid": topicID, "username": username}
	update := bson.M{"$pull": bson.M{"messages": bson.M{"messageid": messageID}}}
	err := queueColection.Update(query, update)
	return err
}

// AddMessageInDLQ ...
func (uc Controller) AddMessageInDLQ(messageModel IMQModel.MessageInfoModel) error {

	err := uc.session.DB("ClientData").C("DLQ").Insert(messageModel)
	return err

}

func undelivered(a, b []string) []string {
	var messageNotDelivered []string
	ma := make(map[string]bool, len(a))
	for _, ka := range a {
		ma[ka] = true
	}
	for _, kb := range b {
		if !ma[kb] {
			messageNotDelivered = append(messageNotDelivered, kb)
		}
	}
	return messageNotDelivered
}
