package storage

import (
	"time"

	"gopkg.in/mgo.v2/bson"
)

// ClientData ...
type ClientData struct {
	SerialNo  bson.ObjectId `json:"serial_no" bson:"serial_no"`
	Timestamp time.Time     `json:"timestamp" bson:"timestamp"`
	Data      string        `json:"data" bson:"data"`
}

// Client ...
type Client struct {
	ClientName string `json:"clientName" bson:"clientName"`
}

// Topic ...
type Topic struct {
	TopicName string `json:"topicname" bson:"topicname"`
}

// MessageModel ...
type MessageModel struct {
	UserName       string   `json:"username" bson:"username"`
	MessageIDArray []string `json:"messageid" bson:"messageid"`
}
