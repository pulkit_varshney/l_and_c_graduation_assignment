package storage_test

import (
	"testing"

	Storage "github.com/pulkitvarshney/l_and_c_graduation_assignment/IMQServer/Storage"
	"gopkg.in/mgo.v2"
)

var clientName = "Tester"
var clientMessage = []string{"Testing the Function"}
var DatabaseConnector = "mongodb://localhost:27017/"

func TestStorageHandler_Success(t *testing.T) {

	session, err := mgo.Dial(DatabaseConnector)
	if err != nil {
		panic(err)
	}
	uc := Storage.DatabaseController(session)
	data := uc.CheckMessageIsAlreadyDeleivered(clientName, clientMessage)
	if data == nil {
		t.Fatalf("err should be nil. got=%v", err)
	}
}
